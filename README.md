# Portfolio - Xavier BRASSOUD

Ce dépôt contient le code source de mon site web www.xavierbrassoud.fr

Les modifications poussées sur la branche **master** sont automatiquement déployées sur mon serveur web.

Le *design* web est basé sur le thème [Vertica][template_theme] proposé par [3Jon][template_author] distribué sous la "[Regular License][template_license]" de [Theme Forest][template_site].


[template_theme]: https://themeforest.net/item/vertica-retina-ready-resume-cv-portfolio/8536870
[template_author]: https://themeforest.net/user/3jon?ref=white_design
[template_license]: https://themeforest.net/licenses/terms/regular
[template_site]: https://themeforest.net